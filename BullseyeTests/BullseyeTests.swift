//
//  BullseyeTests.swift
//  BullseyeTests
//
//  Created by Travis Peebles on 5/20/21.
//

@testable import Bullseye
import XCTest

class BullseyeTests: XCTestCase {
    var game: Game!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before
        // the invocation of each test method in the class.
        game = Game()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after
        // the invocation of each test method in the class.
        game = nil
    }

    func testScorePosative() throws {
        let guess = game.target + 5
        let score = game.points(sliderValue: guess)
        XCTAssertEqual(score, 95)
    }

    func testScoreNegative() throws {
        let guess = game.target - 5
        let score = game.points(sliderValue: guess)
        XCTAssertEqual(score, 95)
    }

    func testNewRound() throws {
        game.startNewRound(points: 100)
        XCTAssertEqual(game.score, 100)
        XCTAssertEqual(game.round, 2)
    }

    func testScoreExact() throws {
        let guess = game.target
        let score = game.points(sliderValue: guess)
        XCTAssertEqual(score, 200)
    }

    func testBonusHalfPosative() throws {
        if game.target == 100 {
            game.target -= 2
        }
        let guess = game.target + 2
        let score = game.points(sliderValue: guess)
        XCTAssertEqual(score, 148)
    }

    func testBonusHalfNegative() throws {
        if game.target == 0 {
            game.target += 2
        }
        let guess = game.target - 2
        let score = game.points(sliderValue: guess)
        XCTAssertEqual(score, 148)
    }

    func testRestart() {
        game.startNewRound(points: 100)
        XCTAssertNotEqual(game.score, 0)
        XCTAssertNotEqual(game.round, 1)
        game.restart()
        XCTAssertEqual(game.score, 0)
        XCTAssertEqual(game.round, 1)
    }
}
