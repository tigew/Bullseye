//
//  BullseyeApp.swift
//  Bullseye
//
//  Created by Travis Peebles on 5/18/21.
//

import SwiftUI

@main
struct BullseyeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
