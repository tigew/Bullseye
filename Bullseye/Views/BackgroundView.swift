//
//  BackgroundView.swift
//  Bullseye
//
//  Created by Travis Peebles on 5/21/21.
//

import SwiftUI

struct BackgroundView: View {
    @State private var resetIsVisable = false
    @Binding var game: Game

    var body: some View {
        VStack {
            TopView(game: $game, resetIsVisible: $resetIsVisable)
            Spacer()
            BottomView(game: $game)
        }
        .padding()
        .background(
            RingsView()
        )
    }
}

struct TopView: View {
    @Binding var game: Game
    @Binding var resetIsVisible: Bool

    var body: some View {
        HStack {
            ResetButton(game: $game, resetIsVisible: $resetIsVisible)
            Spacer()
            RoundedImageViewFilled(systemName: "list.dash")
        }
    }
}

struct NumberView: View {
    var title: String
    var text: String

    var body: some View {
        VStack(spacing: 5) {
            LabelText(text: title.uppercased())
            RoundRectTextView(text: text)
        }

    }
}

struct BottomView: View {
    @Binding var game: Game

    var body: some View {
        HStack {
            NumberView(title: "Score", text: String(game.score))
            Spacer()
            NumberView(title: "Round", text: String(game.round))
        }
    }
}

struct ResetButton: View {
    @Binding var game: Game
    @Binding var resetIsVisible: Bool

    var body: some View {
        Button(action: {
            if game.score > 0 {
                resetIsVisible = true
            }
        }, label: {
            RoundedImageViewStroked(systemName: "arrow.counterclockwise")
        })
        .frame(width: 56.0, height: 56.0)
        .alert(isPresented: $resetIsVisible) {
            Alert(
                title: Text("Reset Game"),
                message: Text("Do you want to reset the game?"),
                primaryButton: .default(
                    Text("No"),
                    action: {}
                ),
                secondaryButton: .destructive(
                    Text("Yes"),
                    action: {game.restart()}
                )
            )
        }
    }
}

struct RingsView: View {

    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        ZStack {
            Color("BackgroundColor")
                .edgesIgnoringSafeArea(.all)
            ForEach(1..<6) { ring in
                let size = CGFloat(ring * 100)
                let opacity = colorScheme == .dark ? 0.1 : 0.3
                Circle()
                    .stroke(lineWidth: 20)
                    .fill(
                        RadialGradient(
                            gradient: Gradient(
                            colors: [Color("RingColor").opacity(opacity),
                                    Color("RingColor").opacity(0)]),
                            center: .center,
                            startRadius: 100, endRadius: 300)
                    )
                    .frame(width: size, height: size, alignment: .center)
            }
        }
    }
}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView(game: .constant(Game()))
        BackgroundView(game: .constant(Game()))
            .preferredColorScheme(.dark)
        RingsView()
        RingsView()
            .preferredColorScheme(.dark)

    }
}
