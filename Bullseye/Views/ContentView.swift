//
//  ContentView.swift
//  Bullseye
//
//  Created by Travis Peebles on 5/18/21.
//

import SwiftUI

struct ContentView: View {
    @State private var alertIsVisable = false
    @State private var sliderValue = 50.0
    @State private var game = Game()

    var body: some View {
        ZStack {
            BackgroundView(game: $game)
            VStack {
                InstructionView(game: $game)
                    .padding(.bottom, alertIsVisable ? 0 : 100)
                if alertIsVisable {
                    PointsView(
                        game: $game,
                        sliderValue: $sliderValue,
                        alertIsVisible: $alertIsVisable
                    )
                        .transition(.scale)
                } else {
                    HitMeButton(
                        alertIsVisable: $alertIsVisable,
                        game: $game,
                        sliderValue: $sliderValue
                    )
                        .transition(.scale)
                }
                
            }
            if !alertIsVisable {
                SliderView(sliderValue: $sliderValue)
                    .transition(.scale)
            }
        }
    }
}

struct InstructionView: View {
    @Binding var game: Game

    var body: some View {
        VStack {
        InstructionText(
            text: "🎯🎯🎯\nPut the Bullseye as close as you can to")
            .padding(.leading, 30.0)
            .padding(.trailing, 30.0)

        BigNumbertext(text: String(game.target))
        }
    }
}

struct SliderView: View {
    @Binding var sliderValue: Double
    var body: some View {
        ZStack {
            HStack {
                SliderLabelText(text: "1")
                Slider(value: $sliderValue,
                       in: 1.0...100.0)
                SliderLabelText(text: "100")
            }.padding()
        }
    }
}

struct HitMeButton: View {
    @Binding var alertIsVisable: Bool
    @Binding var game: Game
    @Binding var sliderValue: Double

    var body: some View {
        Button(
            action: {
                withAnimation {
                    alertIsVisable = true
                }
            },
            label: { Text("Hit Me".uppercased())
                .bold()
                .font(.title3)
            }
        )
        .padding(20.0)
        .background(
            ZStack {
                Color("ButtonColor")
                LinearGradient(
                    gradient: Gradient(colors: [Color.white.opacity(0.3), Color.clear]),
                    startPoint: .top,
                    endPoint: .bottom
                )
            }
        )
        .foregroundColor(Color.white)
        .cornerRadius(Constants.General.roundRectCornerRadius)
        .overlay(
            RoundedRectangle(cornerRadius: Constants.General.roundRectCornerRadius)
                .strokeBorder(Color.white, lineWidth: Constants.General.strokeWidth)
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        ContentView().previewLayout(.fixed(width: 568, height: 320))
        
        ContentView().preferredColorScheme(.dark)
        ContentView().previewLayout(.fixed(width: 568, height: 320))
            .preferredColorScheme(.dark)
    }
}
