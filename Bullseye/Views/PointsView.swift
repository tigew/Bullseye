//
//  PointsView.swift
//  Bullseye
//
//  Created by Travis Peebles on 6/10/21.
//

import SwiftUI

struct PointsView: View {
    @Binding var game: Game
    @Binding var sliderValue: Double
    @Binding var alertIsVisible: Bool
    
    var body: some View {
        VStack(spacing: 10) {
            let roundedValue = Int(sliderValue.rounded())
            let points = game.points(sliderValue: roundedValue)
            
            InstructionText(text: "THE SLIDER'S VALUE IS")
            BigNumbertext(text: String(roundedValue))
            BodyText(text: "You scored \(String(points)) Points\n🎉🎉🎉")
            Button(
                action: {
                    game.startNewRound(points: points)
                    withAnimation {
                        alertIsVisible = false
                    }
                },
                label: {
                    ButtonText(text: "Start New Round")
                }
            )
            
        }
        .padding()
        .frame(maxWidth: 300)
        .background(Color("BackgroundColor"))
        .cornerRadius(Constants.General.roundRectCornerRadius)
        .shadow(radius: 10, x: 5, y: 5)
        .transition(.scale)
    }
}

struct PointsView_Previews: PreviewProvider {
    static var previews: some View {

        
        PointsView(game: .constant(Game()), sliderValue: .constant(5.0), alertIsVisible: .constant(true))
        //PointsView().previewLayout(.fixed(width: 568, height: 320))
        
        //PointsView().preferredColorScheme(.dark)
        //PointsView().previewLayout(.fixed(width: 568, height: 320)).preferredColorScheme(.dark)
    }
}
