//
//  Game.swift
//  Bullseye
//
//  Created by Travis Peebles on 5/20/21.
//

import Foundation

struct Game {
    var target = Int.random(in: 1...100)
    var score = 0
    var round = 1

    func points(sliderValue: Int) -> Int {
        let differance: Int = abs(sliderValue - target)
        var bonusPoints: Int = 0

        if differance == 0 {
            bonusPoints = 100
        } else if differance <= 2 {
            bonusPoints = 50
        }

        return bonusPoints + 100 - abs(sliderValue - target)
    }

    mutating func startNewRound(points: Int) {
        score += points
        round += 1
        target = Int.random(in: 1...100)
    }

    mutating func restart() {
        score = 0
        round = 1
        target = Int.random(in: 1...100)
    }
}
